#include <Arduino.h>
#include <RHDatagram.h>
#include <RH_RF95.h>

#define CLIENT_ADDRESS 1
#define SERVER_ADDRESS 2

#define Serial SerialUSB

RH_RF95 driver(A2, 9);
RHDatagram manager(driver, CLIENT_ADDRESS);

const byte numChars = 32;
char receivedChars[numChars];
char recvLength = 0;

boolean newData = false;

void blink_bool_result(bool status, int blinks)
{
  if (!status)
  {
    while (true)
    {
      for (int x = 0; x < blinks; x++)
      {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(50);
        digitalWrite(LED_BUILTIN, LOW);
        delay(100);
      }
      delay(500);
    }
  }
}

void recvWithStartEndMarkers()
{
  static boolean recvInProgress = false;
  static byte ndx = 0;
  char startMarker = '<';
  char endMarker = '>';
  char rc;

  while (Serial.available() > 0 && newData == false)
  {
    rc = Serial.read();

    if (recvInProgress == true)
    {
      if (rc != endMarker)
      {
        receivedChars[ndx] = rc;
        ndx++;
        if (ndx >= numChars)
        {
          ndx = numChars - 1;
        }
      }
      else
      {
        receivedChars[ndx] = '\0'; // terminate the string
        recvInProgress = false;
        recvLength = ndx;
        ndx = 0;
        newData = true;
      }
    }

    else if (rc == startMarker)
    {
      recvInProgress = true;
    }
  }
}

void showNewData()
{
  if (newData == true)
  {
    uint8_t dst = CLIENT_ADDRESS;

    bool result = manager.sendto((uint8_t *)receivedChars, recvLength, dst);
    blink_bool_result(result, 5);

    Serial.print("#OK|");
    Serial.print(receivedChars);
    Serial.println("#");

    newData = false;
  }
}

void setup()
{
  // serial
  Serial.begin(115200);

  // led
  pinMode(LED_BUILTIN, OUTPUT);

  // init
  driver.waitPacketSent();
  blink_bool_result(manager.init(), 2);

  // set frequency
  blink_bool_result(driver.setFrequency(915.0), 3);

  // set radio preset
  blink_bool_result(driver.setModemConfig(RH_RF95::Bw125Cr45Sf128), 4);

  // set tx power
  driver.setTxPower(20);

  // set modem address
  // manager.setThisAddress(CLIENT_ADDRESS);

  // now we're ready!
  digitalWrite(LED_BUILTIN, HIGH);
  delay(50);
  digitalWrite(LED_BUILTIN, LOW);
}

uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];

void rfRx()
{

  uint8_t len = sizeof(buf);
  uint8_t from;
  uint8_t to;
  uint8_t id;
  uint8_t flags;

  bool result = manager.recvfrom(buf, &len, &from, &to, &id, &flags);

  if (result)
  {
    digitalWrite(LED_BUILTIN, HIGH);

    // the recieved string may not contain a null terminator
    buf[len] = '\0';
    Serial.println((char *)buf);

    digitalWrite(LED_BUILTIN, LOW);
  }
}

void loop()
{
  recvWithStartEndMarkers();
  showNewData();
  rfRx();
}